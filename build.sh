#!/bin/bash
set -euxo pipefail

# Make a note of the package versions.
podman --version
buildah --version

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

# Log into GitLab's container repository.
export REGISTRY_AUTH_FILE=${HOME}/auth.json
echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY

# Set up the container's fully qualified name.
FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}"

# Build the container.
buildah bud -f Dockerfile -t ${IMAGE_NAME} .

# If a tag was provided, use it for the image. If we're on the master branch,
# use 'latest'.
if [ ! -z "${CI_COMMIT_TAG:-}" ]; then
    IMAGE_TAG=${CI_COMMIT_TAG}
elif [ "$CI_COMMIT_REF_NAME" = 'master' ]; then
    IMAGE_TAG="latest"
fi

# For containers that should not be tagged, just update the latest.
if [[ "${IMAGE_TAG:-}" != "" ]] && [[ "${LATEST_ONLY:-}" = 'true' ]]; then
    IMAGE_TAG="latest"
fi

# Push the container if we're building from a tag or from the master branch.
if [ ! -z "${IMAGE_TAG:-}" ]; then
    CONTAINER_ID=$(buildah from ${IMAGE_NAME})
    buildah commit --squash $CONTAINER_ID ${FQ_IMAGE_NAME}:${IMAGE_TAG}
    buildah push ${FQ_IMAGE_NAME}:${IMAGE_TAG}
fi
